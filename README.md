openfire-restapi
================

[![View on PyPI](https://img.shields.io/pypi/v/openfire-restapi.svg?maxAge=2592000)](https://pypi.python.org/pypi/openfire-restapi/)
[![PyPI](https://img.shields.io/pypi/pyversions/openfire-restapi.svg)]()
[![PyPI](https://img.shields.io/pypi/l/openfire-restapi.svg?maxAge=2592000)]()
[![pipeline status](https://gitlab.com/allianceauth/openfire-restapi/badges/master/pipeline.svg)](https://gitlab.com/allianceauth/openfire-restapi/commits/master)
[![coverage report](https://gitlab.com/allianceauth/openfire-restapi/badges/master/coverage.svg)](https://gitlab.com/allianceauth/openfire-restapi/commits/master)


A python client for Openfire's REST API Plugin

Installation
----------------
Install from PyPI:

        pip install openfire-restapi

Install from source:

        $ git clone git://github.com/allianceauth/openfire-restapi.git
        $ cd openfire-restapi
        $ python setup.py install

Documentation
----------------
* [User related](docs/users.md)
* [Groups related](docs/groups.md)
* [Chat room related](docs/muc.md)
* [Session related](docs/sessions.md)
* [Messages related](docs/messages.md)
* [System related](docs/system.md)
